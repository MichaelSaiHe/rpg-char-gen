﻿namespace ImportedCharacter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.classDropdownBox = new System.Windows.Forms.ComboBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.namelabel = new System.Windows.Forms.Label();
            this.wizardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.generateSubmit = new System.Windows.Forms.Button();
            this.statBox = new System.Windows.Forms.RichTextBox();
            this.subclassCheck = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.wizardBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // classDropdownBox
            // 
            this.classDropdownBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classDropdownBox.FormattingEnabled = true;
            this.classDropdownBox.Items.AddRange(new object[] {
            "Wizard",
            "Warrior",
            "Thief"});
            this.classDropdownBox.Location = new System.Drawing.Point(28, 119);
            this.classDropdownBox.Name = "classDropdownBox";
            this.classDropdownBox.Size = new System.Drawing.Size(128, 28);
            this.classDropdownBox.TabIndex = 0;
            this.classDropdownBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.classDropdownBox.TextChanged += new System.EventHandler(this.classDropdownBox_TextChanged);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(28, 71);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(252, 26);
            this.nameBox.TabIndex = 1;
            // 
            // namelabel
            // 
            this.namelabel.AutoSize = true;
            this.namelabel.Location = new System.Drawing.Point(28, 45);
            this.namelabel.Name = "namelabel";
            this.namelabel.Size = new System.Drawing.Size(51, 20);
            this.namelabel.TabIndex = 2;
            this.namelabel.Text = "Name";
            // 
            // wizardBindingSource
            // 
            this.wizardBindingSource.DataSource = typeof(RpgLogic.Wizard);
            // 
            // generateSubmit
            // 
            this.generateSubmit.Location = new System.Drawing.Point(28, 262);
            this.generateSubmit.Name = "generateSubmit";
            this.generateSubmit.Size = new System.Drawing.Size(121, 55);
            this.generateSubmit.TabIndex = 4;
            this.generateSubmit.Text = "Generate";
            this.generateSubmit.UseVisualStyleBackColor = true;
            this.generateSubmit.Click += new System.EventHandler(this.generateSubmit_Click);
            // 
            // statBox
            // 
            this.statBox.Location = new System.Drawing.Point(409, 45);
            this.statBox.Name = "statBox";
            this.statBox.Size = new System.Drawing.Size(273, 272);
            this.statBox.TabIndex = 5;
            this.statBox.Text = "";
            this.statBox.TextChanged += new System.EventHandler(this.statBox_TextChanged);
            // 
            // subclassCheck
            // 
            this.subclassCheck.AutoSize = true;
            this.subclassCheck.Location = new System.Drawing.Point(32, 182);
            this.subclassCheck.Name = "subclassCheck";
            this.subclassCheck.Size = new System.Drawing.Size(100, 24);
            this.subclassCheck.TabIndex = 7;
            this.subclassCheck.Text = "Subclass";
            this.subclassCheck.UseVisualStyleBackColor = true;
            this.subclassCheck.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.subclassCheck);
            this.Controls.Add(this.statBox);
            this.Controls.Add(this.generateSubmit);
            this.Controls.Add(this.namelabel);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.classDropdownBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wizardBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox classDropdownBox;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label namelabel;
        private System.Windows.Forms.BindingSource wizardBindingSource;
        private System.Windows.Forms.Button generateSubmit;
        private System.Windows.Forms.RichTextBox statBox;
        private System.Windows.Forms.CheckBox subclassCheck;
    }
}

