﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RpgLogic;

namespace ImportedCharacter
{
    public partial class Form1 : Form
    {
        Character myCharacter;
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void generateSubmit_Click(object sender, EventArgs e)
        {
            statBox.Text = "";
            if (nameBox.Text == "") { statBox.Text = "Please input a name" + System.Environment.NewLine; }
            if (classDropdownBox.SelectedItem == null) { statBox.Text += "Please select a class"; }
            if (nameBox.Text != "" && classDropdownBox.SelectedItem != null)
            {
                string selected = classDropdownBox.SelectedItem.ToString();
                switch (selected)
                {
                    case "Warrior":
                        if (subclassCheck.Checked) { myCharacter = new Paladin(nameBox.Text, 200, 100, 15, 20, 10, 15); }
                        else { myCharacter = new Warrior(nameBox.Text, 200, 100, 15, 20, 10, 15); }
                        break;
                    case "Wizard":
                        if (subclassCheck.Checked) { myCharacter = new Summoner(nameBox.Text, 200, 100, 15, 20, 10, 15); }
                        else { myCharacter = new Wizard(nameBox.Text, 200, 100, 15, 20, 10, 15); }
                        break;
                    case "Thief":
                        if (subclassCheck.Checked) { myCharacter = new Rogue(nameBox.Text, 200, 100, 15, 20, 10, 15); }
                        else { myCharacter = new Thief(nameBox.Text, 200, 100, 15, 20, 10, 15); }
                        break;
                }
                statBox.Text = "Name: " + myCharacter.Name +
                    System.Environment.NewLine + "HP: " + myCharacter.HP +
                    System.Environment.NewLine + "Mana: " + myCharacter.Mana +
                    System.Environment.NewLine + "Strength: " + myCharacter.Strength +
                    System.Environment.NewLine + "Agility: " + myCharacter.Agility +
                    System.Environment.NewLine + "Intelligence: " + myCharacter.Intelligence +
                    System.Environment.NewLine + "Class: " + (myCharacter.GetType()).ToString().Split('.')[1];


                System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "character.txt", statBox.Text);
            }
        }

        private void statBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void classDropdownBox_TextChanged(object sender, EventArgs e)
        {
            string selected = classDropdownBox.SelectedItem.ToString();
            subclassCheck.Show();
            switch (selected)
            {
                case "Warrior":
                    subclassCheck.Text = "Paladin";
                    break;
                case "Wizard":
                    subclassCheck.Text = "Summoner";
                    break;
                case "Thief":
                    subclassCheck.Text = "Rogue";
                    break;
            }
        }
    }
}
